package TweakNetwork

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

var index = `
<html>
<body>
哈哈
<img src="%v">
</body>
</html>
`
var rc RemoteClass

type RemoteClass interface {
	OnGetQrcode() string
}

func getQrcode(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	bufStr := rc.OnGetQrcode()
	log.Println(bufStr)
	w.Write([]byte(fmt.Sprintf(index, bufStr)))
}

func StartHttpServer(r RemoteClass) {
	rc = r
	router := mux.NewRouter()

	router.HandleFunc("/wxgetqrcode", getQrcode)

	n := negroni.New()

	n.UseHandler(router)

	go n.Run(":8080")
}
