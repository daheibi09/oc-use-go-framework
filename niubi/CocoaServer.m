//
//  CocoaServer.m
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#import "CocoaServer.h"

@implementation RootHTTPConnection
- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path{
    if ([method isEqualToString:@"GET"])
        return YES;
    if ([method isEqualToString:@"POST"])
        return YES;
    if ([method isEqualToString:@"OPTIONS"])
        return YES;
    return [super supportsMethod:method atPath:path];
}
- (void)processBodyData:(NSData *)postDataChunk{
    [request appendData:postDataChunk];
}
- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path{
    if ([path isEqualToString:@"/check"])
    {
        NSString *hellowold = @"OK";
        NSData *responseData = [hellowold dataUsingEncoding:NSUTF8StringEncoding];
        return [[HTTPDataResponse alloc] initWithData:responseData];
    }
    return [super httpResponseForMethod:method URI:path];
}
@end
