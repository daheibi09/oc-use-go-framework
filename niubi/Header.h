//
//  Header.h
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#ifndef Header_h
#define Header_h

#import "Core/HTTPConnection.h"
#import "Core/HTTPResponse.h"
#import "Core/HTTPMessage.h"
#import "Core/Responses/HTTPDataResponse.h"

@interface RootHTTPConnection : HTTPConnection
@end

#endif /* Header_h */
