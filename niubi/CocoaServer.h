//
//  CocoaServer.h
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "Core/HTTPConnection.h"
#import "Core/HTTPResponse.h"
#import "Core/HTTPMessage.h"
#import "Core/Responses/HTTPDataResponse.h"
#import "Core/HTTPServer.h"

NS_ASSUME_NONNULL_BEGIN


@interface CocoaServer : NSObject {
@public
    HTTPServer *httpServer;
}
+ (CocoaServer *)sharedInstance;
- (void)initialize;
- (BOOL)startServer;
- (void)stopServer;
@end

@interface RootHTTPConnection : HTTPConnection {}
@end

NS_ASSUME_NONNULL_END
