//
//  main.m
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
