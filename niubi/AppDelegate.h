//
//  AppDelegate.h
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

