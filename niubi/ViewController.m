//
//  ViewController.m
//  niubi
//
//  Created by tenax09 on 2018/11/4.
//  Copyright © 2018 wocao. All rights reserved.
//

#import "ViewController.h"
#import <TweakNetwork/TweakNetwork.h>

@interface CMyUtils : NSObject
@end

@implementation CMyUtils
+ (NSString *)ToJSON:(id)dict{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    if (error){
        NSLog(@"toJSON error = %@", error);
    }
    NSString *str = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    str = [NSString stringWithFormat:@"%@",str];
    return str;
}
+ (NSDictionary *)FromJSON:(NSString *)arg1{
    NSData *data= [arg1 dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    if (error){
        NSLog(@"fromJSON error = %@", error);
    }
    return (NSDictionary *)jsonObject;
}
+ (UIImage *) dataURL2Image: (NSString *) imgSrc{
    NSURL *url = [NSURL URLWithString: imgSrc];
    NSData *data = [NSData dataWithContentsOfURL: url];
    UIImage *image = [UIImage imageWithData: data];
    return image;
}
@end


@interface ViewController ()<TweakNetworkRemoteClass>
@property (weak, nonatomic) IBOutlet UITextField *orderid;
@property (weak, nonatomic) IBOutlet UITextField *amount;
@property (weak, nonatomic) IBOutlet UIImageView *qrcode;
@property (weak, nonatomic) IBOutlet UITextView *showdata;
@property NSString *qrcodeBuf;
@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    TweakNetworkStartHttpServer(self);
}

- (NSString *) onGetQrcode {
    return self.qrcodeBuf;
}

- (IBAction)jump2weixin:(id)sender {
    NSURL *url = [NSURL URLWithString:@"weixin://"];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}

- (IBAction)getPayQrcode:(id)sender {
    NSLog(@"orderid=%@", _orderid.text);
    NSLog(@"amount=%@", _amount.text);
    NSString *format = @"http://xxxxxxxxxx:39003/getpayqrcode?orderid=%@&amount=%@";
    NSString *urlString = [NSString stringWithFormat:format,_orderid.text, _amount.text];
    NSLog(@"url=%@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *str = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
    NSLog(@"%@",str);
    _showdata.text =str;
    NSDictionary *jsonDict = [CMyUtils FromJSON:str];
    BOOL result = [[jsonDict objectForKey:@"result"] boolValue];
    if (result == true){
        NSString *imgBuf = [jsonDict objectForKey:@"img"];
        NSString *imgHead=@"data:image/png;base64,";
        self.qrcodeBuf = [imgHead stringByAppendingString:imgBuf];
        _qrcode.image = [CMyUtils dataURL2Image:self.qrcodeBuf];
    }
}

@end
